import { catchError } from 'rxjs/operators';
import { AuthService } from './../../shared/auth.service';
import { Component, OnInit } from '@angular/core';
import { EMPTY } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  username = '';
  password = '';

  errorMessage = '';

  constructor(
    private authService: AuthService,
    private router: Router,
  ) { }

  ngOnInit(): void {
  }

  onSubmit() {

    if (!this.username || !this.password) {
      return;
    }

    this.errorMessage = '';

    this.authService.register(this.username, this.password)
      .pipe(
        catchError((err: HttpErrorResponse) => {
          this.errorMessage = err.error ? err.error.message : err.message;
          this.username = '';
          this.password = '';
          return EMPTY;
        })
      )
      .subscribe(res => {
        this.router.navigate(['/auth/login']);
      });
  }
}
