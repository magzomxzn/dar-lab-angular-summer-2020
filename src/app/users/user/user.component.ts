import { UserService } from './../services/user.service';
import { User } from './../../shared/types';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { mergeMap, catchError } from 'rxjs/operators';
import { of } from 'rxjs';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  user: User;

  constructor(
    private route: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.route.data
      .subscribe(({user}) => {
        this.user = user;
      });
  }
}
