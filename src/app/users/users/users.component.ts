import { UserService } from './../services/user.service';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { User } from 'src/app/shared/types';
import { of, Observable } from 'rxjs';
import { Router } from '@angular/router';



@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  users$: Observable<User[]>;

  searchInput = '';

  constructor(
    private userService: UserService,
    private router: Router,
  ) {

  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.users$ = this.userService.getUsers()
      .pipe(
        map(users => this.searchInput ?
          users.filter(user => user.name.includes(this.searchInput) || user.username.includes(this.searchInput))
            : users)
      );
  }

  navigateToUser(id: number) {
    this.router.navigate(['/users', id]);
  }

  search() {
    console.log(this.searchInput);
    this.getData();
  }

  clearSearchInput() {
    this.searchInput = '';
    this.getData();
  }
}
