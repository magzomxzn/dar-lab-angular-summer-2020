import { AuthService } from './../shared/auth.service';
import { Component, OnInit, Input } from '@angular/core';
import { NavItem } from '../shared/types';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Input()
  navItems: NavItem[] = [];

  isLogginIn: boolean;

  constructor(
    private authService: AuthService,
  ) { }

  ngOnInit(): void {
    this.authService.isLoggedIn$
      .subscribe(val => this.isLogginIn = val);
  }


}
